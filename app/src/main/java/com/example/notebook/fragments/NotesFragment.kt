package com.example.notebook.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notebook.NoteEntity
import com.example.notebook.R
import com.example.notebook.activity.NewNoteActivity
import com.example.notebook.activity.ReviewActivity
import com.example.notebook.adapters.RecyclerViewAdapter
import kotlinx.android.synthetic.main.notes_fragment.*
import java.lang.reflect.Method

class NotesFragment() : Fragment() {

    private var notes: List<NoteEntity> = listOf()

    constructor(notes: List<NoteEntity>) : this() {
        this.notes = notes
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.notes_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = RecyclerViewAdapter(notes)
    }



}