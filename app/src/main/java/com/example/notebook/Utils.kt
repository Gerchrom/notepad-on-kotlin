package com.example.notebook


import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class Utils {
    fun Observable<Long>.fromIo() =
        observeOn(Schedulers.computation())
            .subscribeOn(Schedulers.io())

    class TextValidator(var editText: EditText, var text: String) : TextWatcher {
        companion object {
            fun validate(
                firstText: String?,
                secondText: String?
            ) {

            }
        }

        override fun afterTextChanged(s: Editable?) {
            validate(text, editText.text.toString())
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            TODO("Not yet implemented")
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            TODO("Not yet implemented")
        }
    }
}
