package com.example.notebook

import android.content.Context
import android.graphics.Path
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class CircleImageView(context: Context, attrs: AttributeSet?) :
    AppCompatImageView(context, attrs) {
    private var path: Path = Path()
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        path.rewind()
        path.addCircle(width / 2f, height / 2f, 1f, Path.Direction.CCW)
    }

    fun setRadius(radius: Float) {

    }
}