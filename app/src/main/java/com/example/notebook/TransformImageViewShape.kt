package com.example.notebook

abstract class TransformImageShape {

    abstract fun setShapeToImage(shape: ShapeImageView)

    enum class ShapeImageView {
        SQUARE,
        TRIANGLE,
        CIRCLE
    }
}