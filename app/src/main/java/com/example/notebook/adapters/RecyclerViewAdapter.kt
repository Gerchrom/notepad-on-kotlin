package com.example.notebook.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.notebook.NoteEntity
import com.example.notebook.R
import com.example.notebook.activity.NewNoteActivity
import com.example.notebook.activity.ReviewActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.new_note_layout.view.*
import kotlinx.android.synthetic.main.note_item.view.*
import kotlinx.android.synthetic.main.note_item.view.editTextBody
import kotlinx.android.synthetic.main.note_item.view.editTextTitle
import kotlinx.android.synthetic.main.note_item.view.imageView
import java.lang.reflect.Method


open class RecyclerViewAdapter(
    private val notes: List<NoteEntity>

) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.note_item, parent, false)
        val holder = MyHolder(view)
        holder.itemView.setOnClickListener { onClick(parent.context,notes[holder.adapterPosition]) }
        holder.itemView.menuImageView.setOnClickListener {
         showPopupMenu(parent.context,holder.itemView.menuImageView,notes[holder.adapterPosition])
        }
        return holder
    }

    override fun getItemCount(): Int = notes.size


    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.itemView.editTextTitle.text = notes[position].title
        holder.itemView.editTextBody.text = notes[position].body

        if (notes[position].imageName != "") {
            holder.itemView.imageView.setImageURI(Uri.parse(notes[position].imageName))
//            Picasso.with(holder.itemView.context)
//                .load(notes[position].imageName)
//                .into(holder.itemView.imageView)
        } else holder.itemView.imageView.setImageResource(R.drawable.ic_baseline_mood_24)
    }

    class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


   fun onClick(context: Context, note: NoteEntity) {
      context.startActivity(Intent(context, ReviewActivity::class.java).putExtra("Note", note))
    }
    private fun showPopupMenu(context: Context, view: View, note: NoteEntity) {
        val popupMenu = PopupMenu(context, view)

        popupMenu.menuInflater.inflate(R.menu.note_menu, popupMenu.menu)
        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.editMenu -> {
                    context.startActivity(Intent(context, NewNoteActivity::class.java).putExtra("Note", note))
                    true
                }
                R.id.deleteMenu -> {
                    context.startActivity(Intent(context, NewNoteActivity::class.java).putExtra("Note", note))
                    true
                }
                else -> false
            }
        }
        val method: Method = popupMenu.menu.javaClass.getDeclaredMethod(
            "setOptionalIconsVisible",
            Boolean::class.java
        )
        method.invoke(popupMenu.menu, true)
        popupMenu.show()
    }
}
