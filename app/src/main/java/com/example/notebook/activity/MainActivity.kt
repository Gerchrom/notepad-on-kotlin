package com.example.notebook.activity

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.example.notebook.NoteEntity
import com.example.notebook.R
import com.example.notebook.fragments.EmptyFragment
import com.example.notebook.fragments.NotesFragment
import com.example.notebook.viewModel.MainViewModel
import io.reactivex.Observable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var notes: List<NoteEntity>
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = MainViewModel(application)
        viewModel.getAllNotes().observe(this, Observer {
            notes = it
            initFragment()
        })
        supportActionBar?.run {
            setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        application,
                        R.color.toolbar
                    )
                )
            )
        }
    }

    private fun initFragment() {
        val fragment = if (notes.isNotEmpty()) NotesFragment(notes) else EmptyFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete_all_notes -> {
                viewModel.deleteAllNotes(
                    notes,
                    Action {
//                        Toast.makeText(this, "Deleted All Notes", Toast.LENGTH_SHORT).show()
                    }, Consumer {  })
            }
        }
        return true
    }

    //    fun insertNote(): Observable<NoteEntity>{
//        return
//    }
    fun onClickFab(view: View) = startActivity(Intent(this, NewNoteActivity::class.java))
}