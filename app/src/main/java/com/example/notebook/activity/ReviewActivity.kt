package com.example.notebook.activity

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.notebook.NoteEntity
import com.example.notebook.R
import kotlinx.android.synthetic.main.review_note_layout.*

class ReviewActivity : AppCompatActivity() {

    private lateinit var note: NoteEntity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.review_note_layout)
        note = intent.extras?.run { getParcelable<NoteEntity>("Note") }!!
        titleEditText.setText(note.title.toString())
        bodyEditText.setText(note.body.toString())
        imageView.setImageURI(Uri.parse(note.imageName))
        supportActionBar?.run { setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    application,
                    R.color.toolbar
                )
            )
        )
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.review_note_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.edit_note -> {
                startActivity(Intent(applicationContext, NewNoteActivity::class.java).putExtra("Note",note))
            }
            android.R.id.home -> this.finish()
        }
        return true
    }
}