package com.example.notebook.activity

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.notebook.NoteEntity
import com.example.notebook.R
import com.example.notebook.viewModel.NewNoteViewModel
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.new_note_layout.*
import kotlinx.android.synthetic.main.new_note_layout.imageView
import kotlinx.android.synthetic.main.review_note_layout.*

private const val PICK_IMAGE_CODE = 1

class NewNoteActivity : AppCompatActivity() {

    private lateinit var viewModel: NewNoteViewModel
    private lateinit var imageName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_note_layout)
        isCheckNewNote()
        viewModel = NewNoteViewModel(baseContext)
        isCheckFields(editTextTitle, editTextTitle.text.toString())
        isCheckFields(editTextBody, editTextBody.text.toString())
        supportActionBar?.run {
            setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        application,
                        R.color.toolbar
                    )
                )
            )
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    fun onClickImage(view: View) {
        intent = Intent().setAction(Intent.ACTION_PICK).setType("image/*")
        startActivityForResult(intent, PICK_IMAGE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.run { data.data }
            uri?.let {
                viewModel.inputUriToFile(
                    it,
                    contentResolver,
                    filesDir.absolutePath,
                    Consumer { resultUri ->
                        imageView.setImageURI(resultUri)
                        imageName = resultUri.toString()
                    },
                    Consumer { imageView.setImageResource(R.drawable.ic_launcher_background) })
            }
        }
    }

    private fun isCheckFields(editText: EditText, firstText: String) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val secondText = editText.text.toString()
                if (firstText == secondText) {
                    fabInsertNote.hide()
                } else fabInsertNote.show()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> this.finish()
        }
        return true
    }

    fun onClickFab(view: View) {

        viewModel.insert(
            NoteEntity(
                imageName,
                editTextTitle.text.toString(),
                editTextBody.text.toString()
            ), Action { }, Consumer { })

    }

    private fun isCheckNewNote() {
        val note: NoteEntity? = intent.extras?.run { getParcelable<NoteEntity>("Note") }
        if (note != null) {
            editTextTitle.setText(note.title)
            editTextBody.setText(note.body)
            imageView.setImageURI(Uri.parse(note.imageName))
        }
    }

}


