package com.example.notebook.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.notebook.NoteEntity
import io.reactivex.Flowable

@Dao
interface DAO {
    @Query("select * from notes")
    fun getAllNotes(): LiveData<List<NoteEntity>>

    @Delete
    fun delete(allNotesEntity: List<NoteEntity>)

    @Insert
    fun insert(noteEntity: NoteEntity)
}