package com.example.notebook.database

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import com.example.notebook.NoteEntity
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.internal.operators.observable.ObservableOnErrorNext
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class Repository(context: Context) {

    private var database: NoteEntityRoomDatabase = NoteEntityRoomDatabase.getDatabase(context)

    private var notes: LiveData<List<NoteEntity>> = database.userDao().getAllNotes()

    fun getAllNotes(): LiveData<List<NoteEntity>> {
        return notes
    }

    fun deleteAllNotes(
        allNotesEntity: List<NoteEntity>,
        result: Action,
        error: Consumer<Throwable>
    ): Disposable =
        Completable.fromAction { database.userDao().delete(allNotesEntity) }
            .observeOn(Schedulers.computation())
            .subscribeOn(Schedulers.io())
            .subscribe(result, error)

    fun insert(noteEntity: NoteEntity, result: Action, error: Consumer<Throwable>): Disposable =
        Completable.fromAction {
            database.userDao().insert(noteEntity)
        }.observeOn(Schedulers.computation())
            .subscribeOn(Schedulers.io())
            .subscribe(result, error)

    fun inputUriToFile(
        uri: Uri,
        contentResolver: ContentResolver,
        absolutePath: String,
        resultUri: Consumer<Uri>
        , error: Consumer<Throwable>

    ): Disposable = Observable
        .fromCallable {File(absolutePath, System.currentTimeMillis().toString())}
        .observeOn(Schedulers.io())
        .subscribeOn(Schedulers.io())
        .subscribe { file: File ->
            Observable.fromCallable { contentResolver.openInputStream(uri) }
                .map { it.run { readBytes() } }
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe { byteArray: ByteArray ->
                    file.writeBytes(byteArray)
                    Observable.just(file)
                        .map { Uri.fromFile(file) }
                        .observeOn(Schedulers.io())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(resultUri,error)

//                    Observable.just(file.absoluteFile.absolutePath)
//                        .observeOn(Schedulers.computation())
//                        .subscribeOn(Schedulers.io())
//                        .subscribe(onNext)
                }
        }
}


