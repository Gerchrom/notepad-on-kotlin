package com.example.notebook.viewModel

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import androidx.lifecycle.ViewModel
import com.example.notebook.NoteEntity
import com.example.notebook.database.Repository
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer


class NewNoteViewModel(context: Context) : ViewModel() {

    private val repository = Repository(context)

    fun insert(
        noteEntity: NoteEntity,
        result: Action,
        error: Consumer<Throwable>

    ): Disposable {
        saveImageToFile(
            noteEntity
        )
        return repository.insert(noteEntity, result, error)
    }

    fun inputUriToFile(uri: Uri, contentResolver: ContentResolver,filePath: String,resultUri: Consumer<Uri>,error: Consumer<Throwable>) {
        repository.inputUriToFile(uri, contentResolver, filePath,resultUri,error)
    }
}

fun insert() {

}

fun trimImageToSquareImage() {

}


private fun saveImageToFile(
    noteEntity: NoteEntity
) {
}
//    Observable.fromCallable { System.currentTimeMillis() }
//        .observeOn(Schedulers.computation())
//        .subscribeOn(Schedulers.io())
//        .map { File(absoluteFile, System.currentTimeMillis().toString()) }
//        .observeOn(Schedulers.computation())
//        .subscribeOn(Schedulers.io())
//        .subscribe { file: File ->
//            val decoder: BitmapRegionDecoder = BitmapRegionDecoder.newInstance(myStream, false)
//            val region = decoder.decodeRegion(Rect(10, 10, 50, 50), null)
//            noteEntity.imageName = file.absolutePath
//            val bitmap: Bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uriImage)
//            val stream = ByteArrayOutputStream()
//            bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)
//            val image = stream.toByteArray()
//            file.writeBytes(image)



