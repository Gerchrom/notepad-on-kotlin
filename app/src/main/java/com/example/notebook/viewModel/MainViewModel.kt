package com.example.notebook.viewModel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.notebook.NoteEntity
import com.example.notebook.database.Repository
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private var repository: Repository = Repository(application)

    fun getAllNotes(): LiveData<List<NoteEntity>> = repository.getAllNotes()
    fun deleteAllNotes(allNotesEntity: List<NoteEntity>, result: Action, error: Consumer<Throwable>) {

        repository.deleteAllNotes(allNotesEntity, result, error)
    }
}
